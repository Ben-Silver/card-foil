﻿public enum InteractState
{
    PointerDown,
    PointerUp,
    DragStart,
    Dragging,
    DragEnd,
}