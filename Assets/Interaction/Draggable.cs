using UnityEngine;
using UnityEngine.EventSystems;

public class Draggable : Interactable, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    [SerializeField] protected bool dragging = false;
    
    [Header("Draggable Settings")]
    public float holdDistance = 0.1f;
    public float dragSmoothing = 0;

    protected PointerEventData pointerData;
    protected Rigidbody rigidBody;
    protected Vector3 vel = Vector3.zero;

    private Camera _mainCamera;
    protected Camera mainCamera
    {
        get
        {
            if (_mainCamera == null)
                _mainCamera = Camera.main;

            return _mainCamera;
        }
    }
    
    #region Events

    public delegate void Grabbed();
    public event Grabbed OnGrabbed;

    public delegate void Released();
    public event Released OnReleased;

    public delegate void Dragged(PointerEventData _data);
    public event Dragged OnDragged;

    #endregion

    protected override void Start()
    {
        base.Start();
        rigidBody = GetComponent<Rigidbody>();
    }

    #region Interface Implementations

    public void OnBeginDrag(PointerEventData _data)
    {
        if (!interactable)
            return;

        if (pointerID != _data.pointerId)
            return;

        OnInteractEvent?.Invoke(this, InteractState.DragStart, _data);

        dragging = true;
    }

    public void OnDrag(PointerEventData _data)
    {
        if (!interactable)
            return;

        if (pointerID != _data.pointerId)
            return;

        OnInteractEvent?.Invoke(this, InteractState.Dragging, _data);

        Dragging(_data);
    }

    public void OnEndDrag(PointerEventData _data)
    {
        if (!interactable)
            return;

        if (pointerID != _data.pointerId)
            return;

        OnInteractEvent?.Invoke(this, InteractState.DragEnd, _data);

        dragging = false;
    }

    #endregion

    #region Interface Interactors



    protected override void PointerDown(PointerEventData _data)
    {
        pointerData = _data;
        dragging = true;

        PickedUp();
    }

    protected virtual void Dragging(PointerEventData _data)
    {
        pointerData = _data;
        OnDragged?.Invoke(pointerData);
    }

    protected override void PointerUp(PointerEventData _data)
    {
        if (!dragging)
            return;

        dragging = false;

        Dropped();
    }

    #endregion

    #region Pickup & Drop

    public virtual void PickedUp()
    {
        gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");

        SetKinematic(true);
        SetCollisions(false);

        transform.SetParent(null);

        OnGrabbed?.Invoke();
    }

    public virtual void Dropped()
    {
        pointerID = -1; // Clear this to fix issues on iOS
        gameObject.layer = LayerMask.NameToLayer("Interactive");

        SetKinematic(false);
        SetCollisions(true);
        OnReleased?.Invoke();
    }

    public void SetKinematic(bool value)
    {
        if (!rigidBody)
            return;
        rigidBody.isKinematic = value;
    }

    #endregion

    #region Updating

    private void LateUpdate()
    {
        if (dragging)
        {
            DragObject();

            if (!Input.GetMouseButton(0) && !Input.GetMouseButton(1))
            {
                dragging = false;
                Dropped();
            }
        }
    }

    public virtual void DragObject()
    {
        Ray ray = mainCamera.ScreenPointToRay(pointerData.position);

        Vector3 targetPosition = ray.origin + ray.direction * holdDistance;

        transform.rotation = mainCamera.transform.rotation;

        if (dragSmoothing != 0)
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref vel, dragSmoothing * Time.deltaTime);
        else
            transform.position = targetPosition;
    }

    #endregion
}