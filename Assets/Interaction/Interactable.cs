using UnityEngine;
using UnityEngine.EventSystems;

public abstract class Interactable : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    #region Variables

    protected int pointerID = -1;

    [SerializeField] private bool _interactable = true;
    public bool interactable
    {
        get { return _interactable; }
        set
        {
            if (_interactable != value)
                OnInteractableChanged?.Invoke(value);
            
            _interactable = value;
        }
    }

    protected Collider[] col;

    #endregion

    #region Events

    public delegate void InteractableChanged(bool _interactable);
    public InteractableChanged OnInteractableChanged;

    public delegate void InteractEvent(Interactable _interactable, InteractState _action, PointerEventData _data);
    public static InteractEvent OnInteractEvent;

    #endregion

    #region Initialisation

    protected virtual void Start()
    {
        col = GetComponents<Collider>();
    }

    #endregion

    #region Virtual Functions

    protected virtual void PointerDown(PointerEventData _data) { }

    protected virtual void PointerUp(PointerEventData _data) { }

    #endregion

    #region Event Handlers

    public virtual void OnPointerDown(PointerEventData _data)
    {
        if (!interactable)
            return;

        if (pointerID != -1)
            return;

        pointerID = _data.pointerId;

        OnInteractEvent?.Invoke(this, InteractState.PointerDown, _data);

        PointerDown(_data);
    }

    public virtual void OnPointerUp(PointerEventData _data)
    {
        if (!interactable)
            return;

        if (pointerID != _data.pointerId)
            return;

        pointerID = -1;

        OnInteractEvent?.Invoke(this, InteractState.PointerUp, _data);

        PointerUp(_data);
    }

    #endregion

    #region Helpers

    protected void SetCollisions(bool value)
    {
        foreach (var c in col)
            c.enabled = value;
    }

    #endregion
}