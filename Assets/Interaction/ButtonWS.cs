using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ButtonWS : Interactable
{
    #region Variables

    [Header("Button")]
    [Tooltip("Cooldown between each click event you can trigger")]
    public float clickCooldown = 0.1f;

    private float lastTime;

    #endregion

    #region Events

    public UnityEvent OnClickedEvent;

    public delegate void Clicked(ButtonWS _button);
    public event Clicked OnClicked;

    #endregion

    #region Clicking

    protected override void PointerDown(PointerEventData _data)
    {
        if (lastTime != 0)
        {
            if (OnCooldown())
                return;
        }

        Debug.Log($"{gameObject.name} clicked!", this);

        lastTime = Time.time;
        Click();
    }

    public virtual void Click()
    {
        // Event Invocations
        OnClickedEvent?.Invoke();
        OnClicked?.Invoke(this);
    }

    public bool OnCooldown()
    {
        return Time.time - lastTime < clickCooldown;
    }

    #endregion
}