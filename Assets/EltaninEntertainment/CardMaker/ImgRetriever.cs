using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace EltaninEntertainment.CardMaker
{
    using Data;

    public class ImgRetriever : MonoBehaviour
    {
        [Header("JSON")]
        [SerializeField] private TextAsset json;
        [SerializeField] private CardDatas data;
        private int numThreads = 2;
        private int currentThreads = 0;
        private int numberComplete = 0;
        private string directory = "";

        [Header("UI")]
        [SerializeField] private Slider slider;
        [SerializeField] private TMP_Text sliderText;

        [ContextMenu("Debug Directory")]
        private void DebugDirectory()
        {
            Debug.Log($"{Application.persistentDataPath}/CardImages/");
        }

        [ContextMenu("Get Images")]
        public void Get()
        {
            StartCoroutine(GetImages());
        }

        private void Awake()
        {
            data = JsonUtility.FromJson<CardDatas>(json.text);
            directory = $"{Application.persistentDataPath}/CardImages/";

            float percentage = 0.0f / (float)data.Data.Length;
            slider.value = percentage;
            sliderText.text = $"{0} / {data.Data.Length} (0%)";
        }

        private IEnumerator GetImages()
        {
            DateTime startTime = DateTime.Now;

            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            CardData[] cardData = data.Data;
            int length = cardData.Length;

            for (int i = 0; i < length; i++)
            {
                bool exists = File.Exists(directory + cardData[i].ID + ".jpg");
                if (exists)
                {
                    numberComplete++;
                    yield return null;
                }
                else
                {
                    Debug.LogWarning($"{cardData[i].Name} image does not exist!");

                    // Make sure the number of active calls does not exceed the limit
                    yield return new WaitUntil(() => currentThreads < numThreads);
                    Debug.Log($"Getting image for {cardData[i].Name}");

                    yield return GetImage(cardData[i]);
                    Debug.Log($"Finished getting image for {cardData[i].Name}");
                }

                float percentage = (float)numberComplete / (float)length;
                slider.value = percentage;
                sliderText.text = $"{numberComplete} / {length} ({Mathf.RoundToInt(percentage * 100)}%)";
                Debug.Log($"Progress: {sliderText.text}");
            }

            yield return null;

            sliderText.text = "Done!";

            DateTime endTime = DateTime.Now;
            TimeSpan span = endTime - startTime;
            string time = $"Finished in {span.Hours.ToString("00")} hours, {span.Minutes.ToString("00")} minutes, {span.Seconds.ToString("00")} seconds, {span.Milliseconds.ToString()} milliseconds";
            Debug.Log(time);
        }

        public IEnumerator GetImage(CardData _card, Action _onComplete = null)
        {
            currentThreads++;
            yield return GetUtils.Get(_card.CardImages[0].ImageUrlCropped, (output, result) => OnGetTexture(_card, output, result));

            _onComplete?.Invoke();
        }

        private void OnGetTexture(CardData _card, Texture2D _texture, bool _result)
        {
            currentThreads--;
            numberComplete++;

            if (_result)
                SaveTexture(_card.ID, _texture);
            else
                Debug.LogError($"Error occurred attemoting to get {_card.Name}'s image");
        }

        private void SaveTexture(string _name, Texture2D _texture)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
            
            File.WriteAllBytes($"{directory}{_name}.jpg", _texture.EncodeToJPG());
        }
    }
}
