using System;
using UnityEngine;

namespace EltaninEntertainment.CardMaker.Data
{
    [Serializable]
    public class CardDatas
    {
        [SerializeField] private CardData[] data;
        public CardData[] Data { get => data; }
    }

    [Serializable]
    public class CardData
    {
        [SerializeField] private string name;
        public string Name { get => name; }

        [SerializeField] private int id;
        public string ID { get => string.Format("{0:00000000}", id); }

        [SerializeField] private string type;
        public string Type { get => type; }

        [SerializeField] private string frameType;
        public string FrameType { get => frameType; }

        [SerializeField] private string desc;
        public string Description { get => desc; }

        [SerializeField] private int atk;
        public int ATK { get => atk; }

        [SerializeField] private int def;
        public int DEF { get => def; }

        [SerializeField] private int level;
        public int Level { get => level; }

        [SerializeField] private string race;
        public string Race { get => race; }

        [SerializeField] private string attribute;
        public string Attribute { get => attribute; }

        [SerializeField] private string archetype;
        public string Archetype { get => archetype; }

        [SerializeField] private string ygoprodeck_url;
        public string YgoprodeckUrl {  get => ygoprodeck_url; }

        [SerializeField] private CardSet[] card_sets;
        public CardSet[] CardSets { get => card_sets; }

        [SerializeField] private CardImage[] card_images;
        public CardImage[] CardImages { get => card_images; }

        [SerializeField] private CardPrice[] card_prices;
        public CardPrice[] CardPrices { get => card_prices; }
    }

    [Serializable]
    public class CardSet
    {
        [SerializeField] private string set_name;
        public string SetName { get => set_name; }

        [SerializeField] private string set_code;
        public string SetCode { get => set_code; }

        [SerializeField] private string set_rarity;
        public string SetRarity { get => set_rarity; }

        [SerializeField] private string set_rarity_code;
        public string SetRarityCode { get => set_rarity_code; }

        [SerializeField] private string set_price;
        public string SetPrice { get => set_price; }
    }

    [Serializable]
    public class CardImage
    {
        [SerializeField] private int id;
        public int ID { get => id; }

        [SerializeField] private string image_url;
        public string ImageUrl { get => image_url;}

        [SerializeField] private string image_url_small;
        public string ImageUrlSmall {  get => image_url_small; }

        [SerializeField] private string image_url_cropped;
        public string ImageUrlCropped {  get => image_url_cropped; }

    }

    [Serializable]
    public class CardPrice
    {
        [SerializeField] private string cardmarket_price;
        public string CardmarketPrice { get => cardmarket_price; }

        [SerializeField] private string tcgplayer_price;
        public string TCGPlayerPrice {  get => tcgplayer_price; }

        [SerializeField] private string ebay_price;
        public string EbayPrice {  get => ebay_price; }

        [SerializeField] private string amazon_price;
        public string AmazonPrice { get => amazon_price; }

        [SerializeField] private string coolstuffinc_price;
        public string CoolStuffIncPrice {  get => coolstuffinc_price; }
    }
}