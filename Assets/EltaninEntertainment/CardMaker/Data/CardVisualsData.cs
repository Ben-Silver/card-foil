using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.CardMaker.Data
{
    using Commons.Utils;

    [CreateAssetMenu]
    public class CardVisualsData : ScriptableObject
    {
        [EnumNamedArray(typeof(CardType))]
        [SerializeField] private Sprite[] _cardFrames;
        public Sprite[] cardFrames { get => _cardFrames; }

        [EnumNamedArray(typeof(CardAttribute))]
        [SerializeField] private Sprite[] _cardAttributes;
        public Sprite[] cardAttributes { get => _cardAttributes; }

        private string[] _edgeCasesNormal =  { "Normal Tuner Monster" };
        public string[] edgeCasesNormal { get => _edgeCasesNormal; }

        private string[] _edgeCasesEffect =  { "Flip Effect Monster", "Toon Monster", "Tuner Monster", "Union Effect Monster" };
        public string[] edgeCasesEffect { get => _edgeCasesEffect; }

        public Dictionary<string, CardType> GetTypes()
        {
            var types = (CardType[])System.Enum.GetValues(typeof(CardType));
            Dictionary<string, CardType> typesDict = new Dictionary<string, CardType>();

            for (int i = 0; i < types.Length; i++)
            {
                string key = types[i].ToString().Replace('_', ' ');
                typesDict.Add(key, types[i]);
            }

            for (int i = 0; i < edgeCasesNormal.Length; i++)
            {
                string key = edgeCasesNormal[i];
                typesDict.Add(key, CardType.Normal_Monster);
            }

            for (int i = 0; i < edgeCasesEffect.Length; i++)
            {
                string key = edgeCasesEffect[i];
                typesDict.Add(key, CardType.Effect_Monster);
            }

            return typesDict;
        }
    }

    public enum CardType
    {
        Normal_Monster,
        Pendulum_Normal_Monster,
        Effect_Monster,
        Pendulum_Effect_Monster,
        Ritual_Monster,
        Pendulum_Ritual_Monster,
        Fusion_Monster,
        Pendulum_Fusion_Monster,
        Synchro_Monster,
        Pendulum_Synchro_Monster,
        XYZ_Monster,
        Pendulum_XYZ_Monster,
        Link_Monster,
        Spell_Card,
        Trap_Card,
        Token
    }

    public enum CardAttribute
    {
        LIGHT,
        DARK,
        FIRE,
        WATER,
        WIND,
        EARTH,
        DIVINE,
        SPELL,
        TRAP
    }
}
