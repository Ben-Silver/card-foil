using System.Collections.Generic;
using UnityEngine;

namespace EltaninEntertainment.CardMaker.Data
{
    public class CardDataLoader : MonoBehaviour
    {
        [Header("Cards")]
        [SerializeField] private CardContainer cardPrefab;
        [SerializeField] private CardVisualsData cardVisualsData;

        [Header("Rows")]
        [SerializeField] private RectTransform cardRowContainer;
        [SerializeField] private GameObject cardRowPrefab;
        [SerializeField] private int maxCardsPerRow = 5;

        [Header("JSON")]
        [SerializeField] private TextAsset json;
        [SerializeField] private CardDatas data;

        private List<RectTransform> cardRows = new List<RectTransform>();

        private void Start()
        {
            data = JsonUtility.FromJson<CardDatas>(json.text);

            foreach (CardData _card in data.Data)
            {
                Debug.Log($"{_card.Type}");
                //Debug.Log($"{_card.Attribute}");
            }

            int cell = 0;
            for (int i = 0; i < data.Data.Length; i++)
            {
                if (cell == 0)
                {
                    GameObject cardRow = Instantiate(cardRowPrefab, cardRowContainer);
                    cardRows.Add((RectTransform)cardRow.transform);
                }

                CardContainer card = Instantiate(cardPrefab, cardRows[cardRows.Count - 1]);
                card.Initialise(data.Data[i], cardVisualsData);

                cell++;
                if (cell >= maxCardsPerRow)
                    cell = 0;

                if (i == 50)
                    break;
            }
        }
    }
}