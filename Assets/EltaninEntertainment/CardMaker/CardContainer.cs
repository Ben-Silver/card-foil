using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace EltaninEntertainment.CardMaker
{
    using Data;

    public class CardContainer : MonoBehaviour
    {
        [Header("Data")]
        [SerializeField] private CardData card;

        [Header("Text Components")]
        [SerializeField] private RectTransform nameRect;
        [SerializeField] private TMP_Text nameText;
        [SerializeField] private RectTransform typeRect;
        [SerializeField] private TMP_Text typeText;
        [SerializeField] private TMP_Text loreText;
        [SerializeField] private TMP_Text atkText;
        [SerializeField] private TMP_Text defText;

        [Header("Image Components")]
        [SerializeField] private Image frameImage;
        [SerializeField] private Image attributeImage;
        [SerializeField] private RawImage pictureImage;
        
        [Header("Object Components")]
        [SerializeField] private GameObject levelPrefab;
        [SerializeField] private GameObject levelSpacerPrefab;
        [SerializeField] private RectTransform levelContainer;

        public void Initialise(CardData _card)
        {
            card = _card;
            nameText.text = card.Name;
            typeText.text = $"[ {card.Race} ]";
            loreText.text = card.Description;
            atkText.text = card.ATK.ToString();
            defText.text = card.DEF.ToString();

            StartCoroutine(ResizeTexts());
        }

        public void Initialise(CardData _card, CardVisualsData _visuals)
        {
            Initialise(_card);

            Dictionary<string, CardType> types = _visuals.GetTypes();
            CardType type = types[card.Type];
            frameImage.sprite = _visuals.cardFrames[(int)type];

            string directory = $"{Application.persistentDataPath}/CardImages/";
            string fileName = $"{directory}{card.ID}.jpg";
            if (Directory.Exists(directory) && File.Exists(fileName))
            {
                byte[] rawData = File.ReadAllBytes(fileName);
                Texture2D texture = new Texture2D(2, 2);
                texture.name = card.ID;
                texture.LoadImage(rawData);
                pictureImage.texture = texture;
            }

            if (type == CardType.Spell_Card || type == CardType.Trap_Card)
            {
                CardAttribute attribute = type == CardType.Spell_Card ? CardAttribute.SPELL : CardAttribute.TRAP;
                attributeImage.sprite = _visuals.cardAttributes[(int)attribute];

                levelContainer.gameObject.SetActive(false);
                atkText.gameObject.SetActive(false);
                defText.gameObject.SetActive(false);
            }
            else
            {
                CardAttribute attribute = (CardAttribute)System.Enum.Parse(typeof(CardAttribute), card.Attribute);
                attributeImage.sprite = _visuals.cardAttributes[(int)attribute];
                attributeImage.gameObject.SetActive(true);
                levelContainer.gameObject.SetActive(true);
                atkText.gameObject.SetActive(true);
                defText.gameObject.SetActive(true);

                for (int i = 0; i < card.Level; i++)
                {
                    Instantiate(levelPrefab, levelContainer);
                    Instantiate(levelSpacerPrefab, levelContainer);
                }
            }
        }

        private IEnumerator ResizeTexts()
        {
            yield return new WaitForEndOfFrame();

            ResizeText(ref nameText);
            ResizeText(ref typeText);
            ResizeText(ref atkText);
            ResizeText(ref defText);
            ResizeText(ref loreText, true);
            yield return new WaitForEndOfFrame();

            float currentRect = nameText.rectTransform.rect.width;
            float targetRect = nameRect.rect.width;

            if (currentRect > targetRect)
            {
                float delta = targetRect / currentRect;
                nameText.rectTransform.localScale = new Vector3(delta, 1, 1);
            }

            yield break;
        }
        
        private void ResizeText(ref TMP_Text _text, bool _affectLineSpacing = false)
        {
            float cardSizeDelta = ((RectTransform)transform).rect.height / 800;

            float originalSize = _text.fontSize;
            float newSize = originalSize * cardSizeDelta;
            _text.fontSize = newSize;

            if (_affectLineSpacing)
                _text.lineSpacing = newSize;
        }
    }
}