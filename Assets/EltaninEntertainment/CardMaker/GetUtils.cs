using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace EltaninEntertainment
{
    public static class GetUtils
    {
        #region Getting Text

        public static IEnumerator Get(string _url, Action<string, bool> _onComplete = null)
        {
            using (UnityWebRequest www = UnityWebRequest.Get(_url))
            {
                yield return www.SendWebRequest();

                HandleOutput(www, _onComplete);
            }
        }

        public static async Task GetAsync(string _url, Action<string, bool> _onComplete = null)
        {
            using (UnityWebRequest www = UnityWebRequest.Get(_url))
            {
                await www.SendWebRequest();

                HandleOutput(www, _onComplete);
            }
        }

        public static IEnumerator Get(string _url, string _token, Action<string, bool> _onComplete = null)
        {
            using (UnityWebRequest www = UnityWebRequest.Get(_url))
            {
                www.SetRequestHeader("sessionToken", _token);

                yield return www.SendWebRequest();

                HandleOutput(www, _onComplete);
            }
        }

        public static async Task GetAsync(string _url, string _token, Action<string, bool> _onComplete = null)
        {
            using (UnityWebRequest www = UnityWebRequest.Get(_url))
            {
                www.SetRequestHeader("sessionToken", _token);

                await www.SendWebRequest();

                HandleOutput(www, _onComplete);
            }
        }

        public static IEnumerator Get(string _url, string _token, Dictionary<string, object> _params, Action<string, bool> _onComplete = null)
        {
            string json = "{";
            foreach (KeyValuePair<string, object> param in _params)
                json += $"\n\"{param.Key}\": {param.Value}";

            json += "\n}";

            byte[] bodyRaw = Encoding.UTF8.GetBytes(json);

            using (UnityWebRequest www = UnityWebRequest.Get(_url))
            {
                www.SetRequestHeader("sessionToken", _token);
                www.uploadHandler = new UploadHandlerRaw(bodyRaw);
                yield return www.SendWebRequest();

                HandleOutput(www, _onComplete);
            }
        }

        public static async Task GetAsync(string _url, string _token, Dictionary<string, object> _params, Action<string, bool> _onComplete = null)
        {
            string json = "{";
            foreach (KeyValuePair<string, object> param in _params)
                json += $"\n\"{param.Key}\": {param.Value}";

            json += "\n}";

            byte[] bodyRaw = Encoding.UTF8.GetBytes(json);

            using (UnityWebRequest www = UnityWebRequest.Get(_url))
            {
                www.SetRequestHeader("sessionToken", _token);
                www.uploadHandler = new UploadHandlerRaw(bodyRaw);
                await www.SendWebRequest();

                HandleOutput(www, _onComplete);
            }
        }

        #endregion

        #region Getting Images

        public static IEnumerator Get(string _url, Action<Texture2D, bool> _onComplete = null)
        {
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(_url))
            {
                yield return www.SendWebRequest();

                HandleOutput(www, _onComplete);
            }
        }

        public static async Task GetAsync(string _url, Action<Texture2D, bool> _onComplete = null)
        {
            using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(_url))
            {
                await www.SendWebRequest();

                HandleOutput(www, _onComplete);
            }
        }

        #endregion

        #region Output Handling

        private static void HandleOutput(UnityWebRequest _www, Action<string, bool> _onComplete = null)
        {
            if (_www.result == UnityWebRequest.Result.Success)
                _onComplete?.Invoke(_www.downloadHandler.text, true);
            else
                _onComplete?.Invoke(_www.error, false);
        }

        private static void HandleOutput(UnityWebRequest _www, Action<Texture2D, bool> _onComplete = null)
        {
            if (_www.result == UnityWebRequest.Result.Success)
            {
                Texture2D output = ((DownloadHandlerTexture)_www.downloadHandler).texture;
                _onComplete?.Invoke(output, true);
            }
            else
            {
                Debug.LogError(_www.error);
                _onComplete?.Invoke(null, false);
            }
        }

        #endregion

        #region Utils

        public static TaskAwaiter GetAwaiter(this AsyncOperation _operation)
        {
            var tcs = new TaskCompletionSource<object>();
            _operation.completed += obj => { tcs.SetResult(null); };
            return ((Task)tcs.Task).GetAwaiter();
        }

        #endregion
    }
}