using System;
using UnityEngine;

namespace EltaninEntertainment.CardSpawner.Containers
{
    using Data;
    using Enums;

    public class CardContainer : MonoBehaviour
    {
        [SerializeField] private Card card;
        [SerializeField] private float exitDuration;
        private Action exitAction = null;

        public void Initialise(CardData _data)
        {
            card.Set(_data.material);
        }

        public void Initialise(CardData _data, int _index)
        {
            card.Set(_data.rarities[_index]);
        }

        public void DeInitialise(Action _onComplete)
        {
            exitAction = _onComplete;
            card.animator.SetTrigger("Exit");
            Invoke("OnExit", exitDuration);
        }

        private void OnExit()
        {
            exitAction?.Invoke();
            exitAction = null;
        }
    }
}