using System;

namespace EltaninEntertainment.CardSpawner.Enums
{
    [Flags]
    public enum CardRarity : int
    {
        //Nothing = 0,
        Common = 1,
        //Rare = 2,
        Super_Rare = 2,
        Ultra_Rare = 4,
        Secret_Rare = 8,
        Ultimate_Rare = 16
    }
}