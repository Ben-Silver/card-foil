using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EltaninEntertainment.CardSpawner
{
    using Containers;
    using Data;
    using Enums;
    using UI;

    public class CardSpawner : MonoBehaviour
    {
        [Header("Prefab")]
        [SerializeField] private CardContainer cardPrefab;
        private CardData currentCardData = null;
        private CardRarity currentCardRarity = (CardRarity)0;
        private CardContainer currentCard = null;

        [Header("Menu Button")]
        [SerializeField] private Button menuButton;
        private bool menuOpen = false;

        [Header("Menu Panel")]
        [SerializeField] private MenuPanel menuPanel;

        [Header("Data")]
        [SerializeField] private List<CardData> data;
        public Dictionary<string, object> cardData
        {
            get
            {
                Dictionary<string, object> result = new Dictionary<string, object> { { "Select Card", null } };
                for (int i = 0; i < data.Count; i++)
                    result.Add(data[i].cardName, data[i]);
                return result;
            }
        }
        public Dictionary<string, object> rarityData
        {
            get
            {
                Dictionary<string, object> result = new Dictionary<string, object> { { "Select Rarity", 0 } };
                string[] names = Enum.GetNames(typeof(CardRarity));
                var values = Enum.GetValues(typeof(CardRarity));
                for (int i = 0; i < names.Length; i++)
                    result.Add(names[i].Replace("_", " "), values.GetValue(i));
                return result;
            }
        }

        private Coroutine toggleMenu = null;
        private Coroutine error = null;

        private void OnEnable()
        {
            menuButton.onClick.AddListener(ToggleMenu);
            menuPanel.AddListeners(LoadCard, () => UnloadCard(null), SetCardName, SetCardRarity);
            menuPanel.AddDropdownData(cardData, rarityData);
        }

        private void OnDisable()
        {
            menuButton.onClick.RemoveListener(ToggleMenu);
            menuPanel.RemoveListeners();
            menuPanel.RemoveDropdownData();
        }

        private void SetCardName(string _name, object _data)
        {
            currentCardData = _data as CardData;
        }

        private void SetCardRarity(string _name, object _data)
        {
            try
            {
                currentCardRarity = (CardRarity)Enum.Parse(typeof(CardRarity), _data.ToString());
            }
            catch
            {
                currentCardRarity = (CardRarity)0;
            }

            //int data = (int)_data;
            //if (data == 0)
            //    currentCardRarity = (CardRarity)0;
            //else
            //{
            //    int value = Mathf.RoundToInt(Mathf.Pow(2, data - 1));
            //    currentCardRarity = (CardRarity)value;
            //}
        }

        #region Card Management

        private void LoadCard()
        {
            UnloadCard(() =>
            {
                //string cardName = currentCardName.ToLowerInvariant();
                //CardData item = data.Find((i) => i.cardName.ToLowerInvariant() == cardName ||
                //                                 i.cardId.ToLowerInvariant() == cardName);
                if (currentCardData == null)
                {
                    ClearErrorCoroutine();
                    error = StartCoroutine(menuPanel.DisplayError());
                    return;
                }

                int powerOfTwo;
                if (!currentCardData.IsValidRarity(currentCardRarity, out powerOfTwo))
                {
                    ClearErrorCoroutine();
                    error = StartCoroutine(menuPanel.DisplayError());
                    return;
                }

                currentCard = Instantiate(cardPrefab);
                currentCard.Initialise(currentCardData, powerOfTwo);
            });
        }

        private void UnloadCard(Action _onComplete)
        {
            if (currentCard != null)
            {
                currentCard.DeInitialise(() =>
                {
                    Destroy(currentCard.gameObject);
                    currentCard = null;

                    _onComplete?.Invoke();
                });
            }
            else
                _onComplete?.Invoke();
        }

        #endregion

        #region Show/Hide Menu Panel

        private void ToggleMenu()
        {
            menuOpen = !menuOpen;
            ToggleMenu(menuOpen);
        }

        private void ToggleMenu(bool _value)
        {
            ClearToggleCoroutine();
            toggleMenu = StartCoroutine(menuPanel.ToggleMenu(_value ? 0 : 1));
        }

        #endregion

        #region Coroutine Management

        private void ClearToggleCoroutine()
        {
            if (toggleMenu != null)
                StopCoroutine(toggleMenu);
            toggleMenu = null;
        }

        private void ClearErrorCoroutine()
        {
            if (error != null)
                StopCoroutine(error);
            error = null;
        }

        #endregion
    }
}