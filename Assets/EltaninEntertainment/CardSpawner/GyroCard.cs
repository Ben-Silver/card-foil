using UnityEngine;

namespace EltaninEntertainment.Mobile.UI
{
    using Gyro;

    public class GyroCard : GyroHandler
    {
#if UNITY_EDITOR || !UNITY_ANDROID && !UNITY_IOS
        private void Awake()
        {
            useSwipe = true;
        }
#endif

        /// <summary>
        /// Handles proper rotation, and smoothing
        /// </summary>
        protected override void RotateTowards()
        {
            Vector3 euler = gyroRotation.eulerAngles;
            transform.localEulerAngles = new Vector3(-euler.x, -euler.y, transform.eulerAngles.z);
        }
    }
}