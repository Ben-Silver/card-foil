using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace EltaninEntertainment.CardSpawner.UI
{
    using EltaninEntertainment.UI.Elements;

    [Serializable]
    public class MenuPanel
    {
        [SerializeField] private RectTransform menuPanel;
        [SerializeField] private EEDropdown cardDropdown; //TMP_InputField spawnInput;
        [SerializeField] private EEDropdown rarityDropdown;
        [SerializeField] private Button loadButton;
        [SerializeField] private Outline errorOutline;
        [SerializeField] private Button unloadButton;

        public void AddListeners(UnityAction _loadListener, UnityAction _unloadListener, Action<string, object> _inputListener, Action<string, object> _dropdownListener)
        {
            loadButton.onClick.AddListener(_loadListener);
            unloadButton.onClick.AddListener(_unloadListener);
            cardDropdown.OnOptionSelected += _inputListener; //spawnInput.onValueChanged.AddListener(_inputListener);
            rarityDropdown.OnOptionSelected += _dropdownListener; //onValueChanged.AddListener(_dropdownListener);

            //List<string> options = new List<string> { "Select Rarity" };
            //foreach (string option in _dropdownOptions)
            //    options.Add(option.Replace('_', ' '));

            //rarityDropdown.ClearOptions();
            //rarityDropdown.AddOptions(options);
        }

        public void RemoveListeners()
        {
            loadButton.onClick.RemoveAllListeners();
            unloadButton.onClick.RemoveAllListeners();
            cardDropdown.OnOptionSelected = null; //spawnInput.onValueChanged.RemoveAllListeners();
            rarityDropdown.OnOptionSelected = null; //.onValueChanged.RemoveAllListeners();
        }

        public void AddDropdownData(Dictionary<string, object> _inputData, Dictionary<string, object> _dropdownData)
        {
            cardDropdown.Initialise(_inputData);
            rarityDropdown.Initialise(_dropdownData);
        }

        public void RemoveDropdownData()
        {
            cardDropdown.DeInitialise();
            rarityDropdown.DeInitialise();
        }

        public IEnumerator ToggleMenu(float _value)
        {
            while (!Mathf.Approximately(menuPanel.pivot.x, _value))
            {
                menuPanel.pivot = Vector2.Lerp(menuPanel.pivot, new Vector2(_value, menuPanel.pivot.y), 0.1f);
                yield return null;

                if (_value == 0 && menuPanel.pivot.x <= 0.1f)
                    break;
                else if (_value == 1 && menuPanel.pivot.x >= 0.9f)
                    break;
            }

            menuPanel.pivot = new Vector2(_value, menuPanel.pivot.y);
        }

        public IEnumerator DisplayError()
        {
            while (errorOutline.effectDistance.x != 5)
            {
                errorOutline.effectDistance = Vector2.Lerp(errorOutline.effectDistance, Vector2.one * 5, 0.1f);
                yield return null;

                if (errorOutline.effectDistance.x >= 4.9f)
                    break;
            }

            errorOutline.effectDistance = Vector2.one * 5;
            yield return new WaitForSeconds(0.5f);

            while (errorOutline.effectDistance.x != 0)
            {
                errorOutline.effectDistance = Vector2.Lerp(errorOutline.effectDistance, Vector2.zero, 0.1f);
                yield return null;

                if (errorOutline.effectDistance.x < 0.1f)
                    break;
            }
            
            errorOutline.effectDistance = Vector2.zero;
        }
    }
}