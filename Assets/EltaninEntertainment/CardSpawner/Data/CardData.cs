﻿using UnityEngine;

#if UNITY_EDITOR
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
#endif

namespace EltaninEntertainment.CardSpawner.Data
{
    using Enums;

    [CreateAssetMenu]
    public class CardData : ScriptableObject
    {
        [SerializeField] private string _cardId;
        public string cardId { get => _cardId; }

        [SerializeField] private string _cardName;
        public string cardName { get => _cardName; }

        [SerializeField] private Material _material;
        public Material material { get => _material; }

        [SerializeField] private CardRarity _availableRarities;
        public CardRarity availableRarities { get => _availableRarities; }

        [EnumNamedArray(typeof(CardRarity))]
        [SerializeField] private Material[] _rarities;
        public Material[] rarities { get => _rarities; }

        public void Initialise(string _id, Material _material)
        {
            _cardId = _id;
            this._material = _material;
        }

        public bool IsValidRarity(CardRarity _rarity)
        {
            int powerOfTwo = (int)Mathf.Log((int)_rarity, 2);
            return rarities[powerOfTwo] != null;
        }

        public bool IsValidRarity(CardRarity _rarity, out int _powerOfTwo)
        {
            _powerOfTwo = (int)Mathf.Log((int)_rarity, 2);

            if ((int)_powerOfTwo < 0)
                return false;

            return rarities[_powerOfTwo] != null;
        }

#if UNITY_EDITOR
        public void SetRarityValue()
        {
            int rarityValue = 0;

            for (int i = 0; i < rarities.Length; i++)
            {
                if (rarities[i] == null)
                    rarityValue += Mathf.RoundToInt(Mathf.Pow(2, i));
            }

            _availableRarities = (CardRarity)rarityValue;
        }

        public void Convert()
        {
            int length = System.Enum.GetNames(typeof(CardRarity)).Length;
            _rarities = new Material[length];

            //if (rarity.commonMaterial != null)
            //    _rarities[0] = rarity.commonMaterial;

            //if (rarity.superMaterial != null)
            //    _rarities[1] = rarity.superMaterial;

            //if (rarity.ultraMaterial != null)
            //    _rarities[2] = rarity.ultraMaterial;

            //if (rarity.secretMaterial != null)
            //    _rarities[3] = rarity.secretMaterial;

            SetRarityValue();

            EditorUtility.SetDirty(this);
        }

        [MenuItem("Eltanin Entertainment/Card Data/Convert All")]
        public static void ConvertAll()
        {
            CardData[] data = GetCardData();

            foreach (CardData item in data)
            {
                item.Convert();
                AssetDatabase.SaveAssets();
            }

            AssetDatabase.Refresh();
        }

        private static CardData[] GetCardData()
        {
            string[] guids = AssetDatabase.FindAssets("t:CardData");
            CardData[] data = new CardData[guids.Length];

            for (int i = 0; i < guids.Length; i++)
            {
                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                data[i] = AssetDatabase.LoadAssetAtPath<CardData>(path);
            }

            return data;
        }

        [MenuItem("Eltanin Entertainment/Card Data/Create From Material"),
         MenuItem("Assets/Create/Card Data/Create From Material", false, 100)]
        public static void Create()
        {
            Object[] selected = Selection.objects;
            if (selected == null || selected.Length == 0)
                return;

            Material[] materials = GetMaterials(selected);
            foreach (Material material in materials)
            {
                string id = material.name.Split('_').ToList().Find((str) =>
                {
                    string s = str.ToLowerInvariant();
                    return !s.Contains("mtl") &&
                           !s.Contains("super") &&
                           !s.Contains("ultra") &&
                           !s.Contains("secret") &&
                           !s.Contains("ultimate");
                });

                CardData cardData = ScriptableObject.CreateInstance<CardData>();
                cardData.Initialise(id, material);

                string path = AssetDatabase.GetAssetPath(material);
                string newPath = path.Replace($"{material.name}.mat", $"{id}.asset");
                AssetDatabase.CreateAsset(cardData, newPath);
                AssetDatabase.SaveAssets();
            }

            AssetDatabase.Refresh();
        }

        private static Material[] GetMaterials(Object[] _selected)
        {
            List<Material> result = new List<Material>();

            foreach (Object select in _selected)
            {
                if (select.GetType() != typeof(Material))
                    continue;

                result.Add((Material)select);
            }

            return result.ToArray();
        }

        [MenuItem("Eltanin Entertainment/Card Data/Create Material From Texture"),
         MenuItem("Assets/Create/Card Data/Create Material From Texture", false, 100)]
        private static void CreateMaterial()
        {
            Object[] selected = Selection.objects;
            if (selected == null || selected.Length == 0)
                return;

            Texture2D[] textures = GetTextures(selected);
            foreach (Texture2D texture in textures)
            {
                string id = texture.name.Split(' ', '_').ToList().Find((str) =>
                {
                    string s = str.ToLowerInvariant();
                    return !s.Contains("bump") &&
                           !s.Contains("super") &&
                           !s.Contains("ultra") &&
                           !s.Contains("secret") &&
                           !s.Contains("ultimate");
                });

                Shader shader = GetFoilShader("FoilWithTextureLight");
                Texture2D mask = GetDefaultTexture("DefaultBump");
                Texture2D foil = GetDefaultTexture("Untitled-2");

                Material material = new Material(shader);
                material.SetTexture("Image", texture);
                material.SetTexture("Foil", mask);
                material.SetTexture("FoilGradient", foil);
                material.SetFloat("_DirectionalOffset", 1);

                string path = AssetDatabase.GetAssetPath(texture);
                string newPath = path.Replace($"{texture.name}.jpg", $"MTL_{id}.mat");
                AssetDatabase.CreateAsset(material, newPath);
                AssetDatabase.SaveAssets();
            }

            AssetDatabase.Refresh();
        }

        private static Texture2D[] GetTextures(Object[] _selected)
        {
            List<Texture2D> result = new List<Texture2D>();

            foreach (Object select in _selected)
            {
                if (select.GetType() != typeof(Texture2D))
                    continue;

                result.Add((Texture2D)select);
            }

            return result.ToArray();
        }

        private static Texture2D GetDefaultTexture(string _id)
        {
            string[] guids = AssetDatabase.FindAssets("t:Texture2D");
            Texture2D texture = null;

            string[] paths = new string[guids.Length];

            for (int i = 0; i < paths.Length; i++)
                paths[i] = AssetDatabase.GUIDToAssetPath(guids[i]);

            foreach (string path in paths)
            {
                List<string> splitPath = path.Split('/', '.').ToList();
                if (splitPath.Contains(_id))
                    texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
            }

            return texture;
        }

        private static Shader GetFoilShader(string _name)
        {
            string[] guids = AssetDatabase.FindAssets("t:Shader");
            Shader shader = null;

            string[] paths = new string[guids.Length];

            for (int i = 0; i < paths.Length; i++)
                paths[i] = AssetDatabase.GUIDToAssetPath(guids[i]);

            foreach (string path in paths)
            {
                List<string> splitPath = path.Split('/', '.').ToList();
                if (splitPath.Contains(_name))
                {
                    shader = AssetDatabase.LoadAssetAtPath<Shader>(path);
                    break;
                }
            }

            return shader;
        }
#endif
    }
}