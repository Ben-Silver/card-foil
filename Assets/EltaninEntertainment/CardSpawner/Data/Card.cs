﻿using System;
using UnityEngine;

namespace EltaninEntertainment.CardSpawner.Data
{
    [Serializable]
    public class Card
    {
        [SerializeField] private Renderer _renderer;
        public Renderer renderer { get => _renderer; }

        [SerializeField] private Animator _animator;
        public Animator animator { get => _animator; }

        public void Set(Material _material)
        {
            renderer.material = _material;
        }
    }
}